EESchema Schematic File Version 4
LIBS:HeManTCN-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 11
Title "SnifferTCN"
Date "2019-09-25"
Rev "Rev 0.1"
Comp "Gustavo F. Paredes Delaloye"
Comment1 "Tesis para CESE 7ma Cohorte."
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Bombardier:MVBC02D U?
U 1 1 5DA2AA78
P 2450 3650
F 0 "U?" H 2675 664 50  0000 C CNN
F 1 "MVBC02D" H 2675 535 100 0000 C CNN
F 2 "Package_QFP:QFP-100_14x20mm_P0.5mm" H 1650 950 50  0001 R CNN
F 3 "TEXTO" H 1750 6550 50  0001 C CNN
	1    2450 3650
	1    0    0    -1  
$EndComp
$EndSCHEMATC
