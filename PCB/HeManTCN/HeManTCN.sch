EESchema Schematic File Version 4
LIBS:HeManTCN-cache
EELAYER 26 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 11
Title "SnifferTCN"
Date "2019-09-25"
Rev "Rev 0.1"
Comp "Gustavo F. Paredes Delaloye"
Comment1 "Tesis para CESE 7ma Cohorte."
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Graphic:Logo_Open_Hardware_Small LOGO101
U 1 1 5CFBA196
P 14500 6900
F 0 "LOGO101" H 14500 7175 50  0001 C CNN
F 1 "Logo_Open_Hardware_Small" H 14500 6675 50  0001 C CNN
F 2 "footprints:ciaa_7-logo" H 14500 6900 50  0001 C CNN
F 3 "~" H 14500 6900 50  0001 C CNN
	1    14500 6900
	1    0    0    -1  
$EndComp
$Comp
L Graphic:Logo_Open_Hardware_Small LOGO102
U 1 1 5CFBA47B
P 15200 6900
F 0 "LOGO102" H 15200 7175 50  0001 C CNN
F 1 "Logo_Open_Hardware_Small" H 15200 6675 50  0001 C CNN
F 2 "footprints:unqui-logo" H 15200 6900 50  0001 C CNN
F 3 "~" H 15200 6900 50  0001 C CNN
	1    15200 6900
	1    0    0    -1  
$EndComp
Text HLabel 13100 7150 0    50   Input ~ 0
LabelHierychal
$Sheet
S 10050 1650 1100 900 
U 5D9FB101
F0 "redTCN" 50
F1 "redTCN.sch" 50
$EndSheet
Text HLabel 13500 7750 0    50   Input ~ 0
LabelHier
$Sheet
S 2100 2450 500  850 
U 5DA2C15A
F0 "CPU" 50
F1 "CPU.sch" 50
$EndSheet
$Sheet
S 3450 1150 900  900 
U 5DA4186F
F0 "Memoria Externa" 50
F1 "Memoria Externa.sch" 50
$EndSheet
$Sheet
S 9450 5400 800  1100
U 5DA4DC5D
F0 "USB" 50
F1 "USB.sch" 50
$EndSheet
$Sheet
S 3050 4550 1000 1300
U 5DA532BE
F0 "Protoboard" 50
F1 "Protoboard.sch" 50
$EndSheet
$Sheet
S 1850 8250 1000 1400
U 5DA5A719
F0 "Ethernet" 50
F1 "Ethernet.sch" 50
$EndSheet
$Sheet
S 9100 7800 750  850 
U 5DA5E268
F0 "Power" 50
F1 "Power.sch" 50
$EndSheet
$Sheet
S 10800 8300 750  800 
U 5DA5F3DC
F0 "User Interface" 50
F1 "User Interface.sch" 50
$EndSheet
$Sheet
S 10950 4050 700  1000
U 5DA5FACD
F0 "RS485" 50
F1 "RS485.sch" 50
$EndSheet
$Sheet
S 13150 3750 900  1000
U 5DA600DB
F0 "Debug & Serial" 50
F1 "Debug-Serial.sch" 50
$EndSheet
$EndSCHEMATC
